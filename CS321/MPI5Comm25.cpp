#include "pt4.h"
#include "mpi.h"
void Solve()
{
    Task("MPI5Comm25");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	double a;
	if (rank != 0) {
		pt >> a;
	}

	int dims[1] = { size };
	int periods[1] = { -1 };
	MPI_Comm comm_cart;
	MPI_Cart_create(MPI_COMM_WORLD, 1, dims, periods, 0, &comm_cart);

	int rank_in;
	int rank_out;
	MPI_Cart_shift(comm_cart, 0, -1, &rank_in, &rank_out);

	double b;
	if (rank == 0) {
		MPI_Recv(&b, 1, MPI_DOUBLE, rank_in, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
		pt << b;
	}
	else if (rank == size - 1) {
		MPI_Send(&a, 1, MPI_DOUBLE, rank_out, 0, MPI_COMM_WORLD);
	}
	else {
		MPI_Send(&a, 1, MPI_DOUBLE, rank_out, 0, MPI_COMM_WORLD);
		MPI_Recv(&b, 1, MPI_DOUBLE, rank_in, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
		pt << b;
	}
}
