#include "pt4.h"
#include "mpi.h"
void Solve()
{
    Task("MPI5Comm10");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int n;
	pt >> n;

	int a;
	pt >> a;

	MPI_Comm comm_1;
	MPI_Comm comm_2;
	MPI_Comm target_comm;
	int color_1, color_2;
	if (n == 1) {
		color_1 = 0;
		color_2 = MPI_UNDEFINED;
	}
	else {
		color_1 = MPI_UNDEFINED;
		color_2 = 0;
	}
	MPI_Comm_split(MPI_COMM_WORLD, color_1, rank, &comm_1);
	MPI_Comm_split(MPI_COMM_WORLD, color_2, rank, &comm_2);
	if (n == 1)
		target_comm = comm_1;
	else
		target_comm = comm_2;

	MPI_Group grp;
	MPI_Comm_group(target_comm, &grp);
	int grp_size;
	MPI_Group_size(grp, &grp_size);

	int* rbuf = new int[grp_size];
	MPI_Status stat;
	MPI_Allgather(&a, 1, MPI_INT, rbuf, 1, MPI_INT, target_comm);
	for (int i = 0; i < grp_size; i++)
		pt << rbuf[i];
}
