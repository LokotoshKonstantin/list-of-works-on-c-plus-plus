#include "pt4.h"
#include "omp.h"


double classic(double x, int n)
{
  double external_sum = 0.;
  for (int i = 1; i <= n; i++) {
    double internal_sum = 0;
    for (int j = i; j <= n; j++)
      internal_sum += (j + pow(x + j, 1. / 3)) / (2 * i * j - 1);
    external_sum += 1. / internal_sum;
  }
  return external_sum; 
}

double parallel(double x, int n)
{
  double external_sum = 0.;
  #pragma omp parallel num_threads(2) reduction(+:external_sum)
  {
    int thread_num = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
    int num_procs = omp_get_num_procs();
    int cnt = 0;
    if (thread_num == 0) {
      Show("\nnum_procs: ");
      Show(num_procs);
      Show("\nnum_threads: ");
      Show(num_threads);
    }
    double start = omp_get_wtime();
    for (int i = thread_num; i <= n; i += 2) {
      double internal_sum = 0.;
      for (int j = i; j <= n; j++) {
        internal_sum += (j + pow(x + j, 1. / 3)) / (2 * i * j - 1);
        cnt++;
      }
      external_sum += 1.0 / internal_sum;
    }
    Show("\nthread_num:");
    Show(thread_num);
    Show("count:");
    Show(cnt);
    Show("thread time:");
    Show(omp_get_wtime() - start);
  }
  return external_sum;
}

void Solve()
{
    Task("OMPBegin3");
    double x;
    int n;
    double classic_took, parallel_took;
    pt >> x;
    pt >> n;
    double start = omp_get_wtime();
    pt << classic(x, n);
    classic_took = omp_get_wtime() - start;
    Show("Non-parallel time:");
    Show(classic_took);
    pt >> x;
    pt >> n;
    start = omp_get_wtime();
    pt << parallel(x, n);
    parallel_took = omp_get_wtime() - start;
    Show("\nTotal parallel time:");
    Show(parallel_took);
    Show("\nRate:");
    Show(classic_took / parallel_took);
}
