#include "pt4.h"
#include "mpi.h"
void Solve()
{
    Task("MPI5Comm28");
    int flag;
    MPI_Initialized(&flag);
    if (flag == 0)
        return;
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int k = size;
	int n = (k - 1) / 2;

	int* indexes = new int[k];
	int* edges = new int[n * 2 * 2];

	indexes[0] = n;
	int counter = 0;
	for (int i = 1; i < size; i += 2)
		edges[counter++] = i;

	int add = 2;
	int t = -1;
	for (int i = 1; i < size; i++) {
		indexes[i] = indexes[i - 1] + add;
		add += t;

		if (t < 0) {
			edges[counter++] = 0;
			edges[counter++] = i + 1;
		}
		else
			edges[counter++] = i - 1;

		t *= -1;
	}

	MPI_Comm g_comm;
	MPI_Graph_create(MPI_COMM_WORLD, size, indexes, edges, 0, &g_comm);

	int count;
	MPI_Graph_neighbors_count(g_comm, rank, &count);
	int* neighbors = new int[count];
	MPI_Graph_neighbors(g_comm, rank, count, neighbors);
	int a, b;
	MPI_Status s;	pt >> a;

	if (rank % 2 == 0)
		for (int i = 0; i < count; i++) {
			MPI_Send(&a, 1, MPI_DOUBLE, neighbors[i], 0, g_comm);
			MPI_Recv(&b, 1, MPI_DOUBLE, neighbors[i], 0, g_comm, MPI_STATUSES_IGNORE);
			pt << b;
		}
	else 
		for (int i = 0; i < count; i++) {
			MPI_Recv(&b, 1, MPI_DOUBLE, neighbors[i], 0, g_comm, MPI_STATUSES_IGNORE);
			MPI_Send(&a, 1, MPI_DOUBLE, neighbors[i], 0, g_comm);	
			pt << b;
		}
	
}
