#include <iostream>
#include <string>
#include <cmath> 
#include <algorithm>
#include <vector>
#include <numeric>
#include <map>

// Задание

/*
Реализуйте систему хранения автобусных маршрутов. Вам нужно обрабатывать следующие запросы:

-NEW_BUS bus stop_count stop1 stop2 ... — добавить маршрут автобуса с названием bus и stop_count остановками с названиями stop1, stop2, ...
-BUSES_FOR_STOP stop — вывести названия всех маршрутов автобуса, проходящих через остановку stop.
-STOPS_FOR_BUS bus — вывести названия всех остановок маршрута bus со списком автобусов, на которые можно пересесть на каждой из остановок.
-ALL_BUSES — вывести список всех маршрутов с остановками.

Формат ввода

В первой строке ввода содержится количество запросов Q, затем в Q строках следуют описания запросов.

Гарантируется, что все названия маршрутов и остановок состоят лишь из латинских букв, цифр и знаков подчёркивания.

Для каждого запроса NEW_BUS bus stop_count stop1 stop2 ... гарантируется, что маршрут bus отсутствует,
количество остановок больше 0, а после числа stop_count следует именно такое количество названий остановок,
 причём все названия в каждом списке различны.
Формат вывода

Для каждого запроса, кроме NEW_BUS, выведите соответствующий ответ на него:

-На запрос BUSES_FOR_STOP stop выведите через пробел список автобусов, проезжающих через эту остановку,
 в том порядке, в котором они создавались командами NEW_BUS. Если остановка stop не существует, выведите No stop.

-На запрос STOPS_FOR_BUS bus выведите описания остановок маршрута bus в отдельных строках в том порядке,
в котором они были заданы в соответствующей команде NEW_BUS. Описание каждой остановки stop должно иметь вид Stop stop:
 bus1 bus2 ..., где bus1 bus2 ... — список автобусов, проезжающих через остановку stop, в порядке,
 в котором они создавались командами NEW_BUS, за исключением исходного маршрута bus.
Если через остановку stop не проезжает ни один автобус, кроме bus, вместо списка автобусов для неё выведите no interchange.
Если маршрут bus не существует, выведите No bus.

-На запрос ALL_BUSES выведите описания всех автобусов в алфавитном порядке. Описание каждого маршрута bus
должно иметь вид Bus bus: stop1 stop2 ..., где stop1 stop2 ... — список остановок автобуса bus в порядке,
 в котором они были заданы в соответствующей команде NEW_BUS. Если автобусы отсутствуют, выведите No buses.
*/

using namespace std;

int main() {
  int N;
  std::cin >> N;

  map<string, vector<string>> world{};
  vector<string> vector_bus{};
  
  for (int i = 0; i < N; i++) {
    string command;
    std::cin >> command;
    if (command == "NEW_BUS") {
      string bus;
      int stop_count;
      std::cin >> bus >> stop_count;
      vector<string> vector_stations{};
      for (int i = 0; i < stop_count; i++) {
        string station;
        std::cin >> station;
        vector_stations.push_back(station);
      }
      vector_bus.push_back(bus);
      world[bus] = vector_stations;
    }

    if (command == "BUSES_FOR_STOP") {
      string stop;
      std::cin >> stop;

      // Цикл по элементам map, в котором мы проверяем, есть ли данная остановка в маршруте автобуса. Если остановка есть, то добавляем название автобуса
      //в вектор exist_bus, чтобы после пройтись по вектору vector_bus, и если нашли наш автобус в векторе exist_bus, то выводим название автобуса.
      vector<string> exist_bus;
      vector<string> exist_bus_print;

      for (auto item : world) {
        if (std::find(item.second.begin(), item.second.end(), stop) != item.second.end()) {
          exist_bus.push_back(item.first);
        }
      }

      // Если станции не оказалась в маршрутах автобусов
      if (exist_bus.empty()) {
        std::cout << "No stop" << std::endl;
      }

      // Если мы нашли автобусы, в маршрутах которых есть данная станция.
      else {
        for (auto item : vector_bus) {
          if (std::find(exist_bus.begin(), exist_bus.end(), item) != exist_bus.end()) {
            exist_bus_print.push_back(item);
          }
        }
        for (auto item : exist_bus_print) {
          std::cout << item << " ";
        }
        std::cout << std::endl;
      }
    }

    if (command == "ALL_BUSES") {
      if (world.empty()) {
        std::cout << "No buses" << std::endl;
      }
      else {
        for (auto item : world) {
          std::cout << "Bus " << item.first << ": ";

          for (auto station : item.second) {
            std::cout << station << " ";
          }
          std::cout << std::endl;
        }
        std::cout << std::endl;
      }

    }

    if (command == "STOPS_FOR_BUS") {
      string bus;
      std::cin >> bus;

      if (!world.count(bus)) {
        std::cout << "No bus" << std::endl;
      }
      else {
        for (auto item : world[bus]) {
          std::cout << "Stop " + item + ": ";

          vector<string> exist_bus;
          vector<string> exist_bus_print;

          for (auto t : world) {
            if (std::find(t.second.begin(), t.second.end(), item) != t.second.end()){
              exist_bus.push_back(t.first);
            }
          }
          // Если мы нашли автобусы, в маршрутах которых есть данная станция.
          for (auto item : vector_bus) {
            if (std::find(exist_bus.begin(), exist_bus.end(), item) != exist_bus.end()) {
              exist_bus_print.push_back(item);
            }
          }

          exist_bus_print.erase(std::remove(exist_bus_print.begin(), exist_bus_print.end(), bus), exist_bus_print.end());

          if (exist_bus_print.empty()) {
            std::cout << "no interchange";
          }
          else {
            for (auto item : exist_bus_print) {
              std::cout << item << " ";
            }
          }
          std::cout << std::endl;
        }
      }

    }
  }
  return 0;
}
