#include <iostream>
#include <string>
#include <cmath> 
#include <algorithm>
#include <vector>
#include <numeric>
#include <map>
#include <set>
#include <tuple>

// Задание

/*
Считайте, что в каждый год может произойти не более одного изменения фамилии и не более одного изменения имени. 
При этом с течением времени могут открываться всё новые факты из прошлого человека, 
поэтому года́ в последовательных вызовах методов ChangeLastName и ChangeFirstName не обязаны возрастать.

Гарантируется, что все имена и фамилии непусты.

Строка, возвращаемая методом GetFullName, должна содержать разделённые одним пробелом имя и фамилию человека по состоянию на конец данного года.

-Если к данному году не случилось ни одного изменения фамилии и имени, верните строку "Incognito".
-Если к данному году случилось изменение фамилии, но не было ни одного изменения имени, верните "last_name with unknown first name".
-Если к данному году случилось изменение имени, но не было ни одного изменения фамилии, верните "first_name with unknown last name".

В отличие от метода GetFullName, метод GetFullNameWithHistory должен вернуть не только последние имя и фамилию к концу данного года,
 но ещё и все предыдущие имена и фамилии в обратном хронологическом порядке. Если текущие факты говорят о том, 
 что человек два раза подряд изменил фамилию или имя на одно и то же, второе изменение при формировании истории нужно игнорировать.
 */

using namespace std;

string returnStringFromHistory(const vector<string>& value) {
  auto first = true;
  string result = "(";
  for (const auto& item : value) {
    if (!first) {
      result += ", ";
    }
    first = false;
    result += item;    
  }
  result += ")";
  return result;
}

class Person
{
public:
  void ChangeFirstName(int year, string first_name) {
    changes_first[year] = std::move(first_name);
  }

  void ChangeLastName(int year, string last_name) {
    changes_last[year] = std::move(last_name);
  }

  string GetFullName(int year) {
    string first{}, last{};

    for (const auto& item : changes_first) {
      if (item.first <= year) {
        last = item.second;
      }
    }

    for (const auto& item : changes_last) {
      if (item.first <= year) {      
          last = item.second;
      }
    }

    if (first.empty() && last.empty()) {
      return "Incognito";
    }
    else if (first.empty()) {
      return first + " with unknown first name";
    }
    else if (last.empty()) {
      return last + " with unknown last name";
    }
    else {
      return first + " " + last;
    }
  }

  string GetFullNameWithHistory(int year) {
    tuple<string, string> current_person = std::make_tuple("", "");

    bool change_first = false;
    bool change_last = false;

    vector<string> history_first;
    vector<string> history_last;

    for (auto& item : changes_first) {
      if (item.first <= year) {
        std::get<0>(current_person) = item.second;

        if (history_first.empty()) {
          history_first.push_back(item.second);
        }
        else {
          if (history_first[history_first.size() - 1] != item.second) {
            history_first.push_back(item.second);
          }
        }

        change_first = true;
      }
    }

    for (auto& item : changes_last) {
      if (item.first <= year) {
        std::get<1>(current_person) = item.second;

        if (history_last.empty()) {
          history_last.push_back(item.second);
        }
        else {
          if (history_last[history_last.size() - 1] != item.second) {
            history_last.push_back(item.second);
          }
        }

        change_last = true;
      }
    }

    std::reverse(history_first.begin(), history_first.end());
    std::reverse(history_last.begin(), history_last.end());

    if (!history_first.empty()) {
      history_first.erase(history_first.begin());
    }
    
    if (!history_last.empty()) {
      history_last.erase(history_last.begin());
    }

    if (change_first == false and change_last == false) {
      return "Incognito";
    }
    else if (change_first == false and change_last == true) {
      if (history_last.empty()) {
        return std::get<1>(current_person) + " with unknown first name";
      }
      else {
        return std::get<1>(current_person) + " " + returnStringFromHistory(history_last) + " with unknown first name";
      }

    }
    else if (change_first == true and change_last == false) {
      if (history_first.empty()) {
        return std::get<0>(current_person) + " with unknown last name";
      }
      else {
        return std::get<0>(current_person) + " " + returnStringFromHistory(history_first) + " with unknown last name";
      }
    }
    else {
      if (history_first.empty() and history_last.empty()) {
        return std::get<0>(current_person) + " " + std::get<1>(current_person);
      }
      else if (history_first.empty() and !history_last.empty()) {
        return std::get<0>(current_person) + " " + std::get<1>(current_person) + " " + returnStringFromHistory(history_last);
      }
      else if (!history_first.empty() and history_last.empty()) {
        return std::get<0>(current_person) + " " + returnStringFromHistory(history_first) + " " + std::get<1>(current_person);
      }
      else {
        return std::get<0>(current_person) + " " + returnStringFromHistory(history_first) + " " + std::get<1>(current_person) + " " + returnStringFromHistory(history_last);
      }
    }
  }
private:
  map<int, string> changes_first;
  map<int, string> changes_last;
};

int main() {
  Person person;

  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year : {1900, 1965, 1990}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1970, "Appolinaria");
  for (int year : {1969, 1970}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1968, "Volkova");
  for (int year : {1969, 1970}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1990, "Polina");
  person.ChangeLastName(1990, "Volkova-Sergeeva");
  cout << person.GetFullNameWithHistory(1990) << endl;

  person.ChangeFirstName(1966, "Pauline");
  cout << person.GetFullNameWithHistory(1966) << endl;

  person.ChangeLastName(1960, "Sergeeva");
  for (int year : {1960, 1967}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1961, "Ivanova");
  cout << person.GetFullNameWithHistory(1967) << endl;
  
  return 0;
}


