#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <chrono>
#include <set>
using namespace std;

vector<string> floors;
vector<vector<size_t>> weights;
static size_t n = 0;
static size_t m = 0;
static size_t k = 0;
static size_t w = 0;

typedef enum{
    /*invalid input file format*/
    status_input_file_error = 0,
    /*success*/
    status_success = 1,
    /*invalid number of args*/
    status_arguments_error = 2,
    /*invalid output file*/
    status_output_file_error = 3
}status;

/**
 * @brief fill_matrix Fills matrices
 * @param input_file opened ifstream
 * @param matrix_num bool, 0 for Tecey matrix, 1 for Peyrifoy matrix
 * @return status
 */
status fill_matrix(ifstream & input_file)
{
    string empty = "";
    getline(input_file, empty);
    for(size_t floor = 0; floor < k; floor++)
    {
        weights[floor] = vector<size_t>(k);
        string buf = "";
        string tmp = "";
        for(size_t i = 0; i < n; i++)
        {
            getline(input_file, tmp);
            buf += tmp;
            tmp = "";
        }
       floors[floor] = buf;
       buf = "";
    }
    return status_success;
}

/**
 * @brief reads all data from input file
 * @param path_to_file path to input file
 * @return status
 */
status get_input(string &path_to_file)
{
    ifstream input_file(path_to_file);
    if(input_file.is_open())
    {
        input_file >> n >> m >> k >> w;
        floors = vector<string>(k);
        weights = vector<vector<size_t>>(k);
        fill_matrix(input_file);
    }
    input_file.close();
    return status_success;
}

status get_output(string &path_to_file, size_t &score, vector<pair<size_t, size_t>> &result)
{
    ofstream output_file(path_to_file);
    if(output_file.is_open())
    {
        output_file << score << endl;
        for(auto &x: result)
            output_file << x.first << " " << x.second << endl;
        output_file.close();
        return status_success;
    }
    else return status_output_file_error;
}


size_t diff(string &s1, string &s2)
{
    size_t result = 0;
    for(size_t i = 0; i < s1.length(); i++)
        if(s1[i] != s2[i])
        {
            result ++;
            if(w*result >= n*m)
                return n*m;
        }
    return w*result;
}
int get_weights()
{
    for(size_t i = 0; i < k; i++)
        for(size_t j = 0; j < k; j++)
        {
            if(i != j)
            {
                size_t difference = diff(floors[i], floors[j]);
                weights[i][j] = difference;
                weights[j][i] = difference;
            }
        }
    return 0;
}

size_t min_key(const vector<size_t> &key, const vector<bool> &mst)
{
    size_t min = numeric_limits<size_t>::max();
    size_t min_index = 0;
    for(size_t index = 0; index < k; index++)
        if(!mst[index] && key[index] < min)
        {
            min = key[index];
            min_index = index;
        }
    return min_index;
}

size_t MST(vector<pair<size_t, size_t>>& result)
{
    size_t score = 0;
    vector<bool> mst(k, false);
    vector<int> parent(k, -1);
    vector<size_t> key(k, numeric_limits<size_t>::max());
    key[0] = 0;
    parent[0] = -1;
    result.emplace_back(make_pair(1,0));
    score += n*m;
    for(size_t count = 0; count < k; count++)
    {
        size_t u = min_key(key, mst);
        mst[u] = true;
        if(parent[u] != -1)
        {
            score += key[u];
            if (key[u] == n*m)
                result.emplace_back(make_pair(u+1, 0));
            else
                result.emplace_back(make_pair(u+1, parent[u] + 1));
        }
        for(size_t v = 0; v < k; v++)
            if(!mst[v] && weights[u][v] < key[v])
            {
                parent[v] = static_cast<int>(u);
                key[v] = weights[u][v];
            }
    }
    return score;
}

int main(/*int argc, char** argv*/)
{
//    string path_to_input = argv[1];
//    string path_to_output = argv[2];
    string path_to_input = "input.txt";
    string path_to_output = "output.txt";
    get_input(path_to_input);
    get_weights();
    vector<pair<size_t, size_t>> result;
    size_t score = MST(result);
    get_output(path_to_output, score, result);
    return 0;
}
