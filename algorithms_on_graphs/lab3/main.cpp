#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <regex>
#include <algorithm>

using namespace std;

vector<vector<double>> matrix;
vector<size_t> restored_way;
vector<pair<size_t, size_t>> result;
static size_t n = 0;

typedef enum{
    /*invalid input file format*/
    status_input_file_error = 0,
    /*success*/
    status_success = 1,
    /*invalid number of args*/
    status_arguments_error = 2,
    /*invalid output file*/
    status_output_file_error = 3
}status;

/**
 * @brief splits string into array of string
 * @param str string
 * @param delimiter delimiter
 * @return
 */
vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;

  while(getline(ss, tok, delimiter)) {
    internal.emplace_back(tok);
  }
  return internal;
}

/**
 * @brief fill_matrix Fills matrices
 * @param input_file opened ifstream
 * @param matrix_num bool, 0 for Tecey matrix, 1 for Peyrifoy matrix
 * @return status
 */
status fill_matrix(ifstream & input_file)
{
    for(size_t i = 0; i < n; i++)
    {
        for(size_t j = 0; j < n; j++)
        {
            double elem;
            input_file >> elem;
            matrix[i].emplace_back(elem);
        }
    }
    return status_success;
}

/**
 * @brief reads all data from input file
 * @param path_to_file path to input file
 * @return status
 */
status get_input(string &path_to_file)
{
    ifstream input_file(path_to_file);
    if(input_file.is_open())
    {
        input_file >> n;
        /*memory allocation for matrices*/
        matrix = vector<vector<double>>(n);
        restored_way = vector<size_t>(n);
        for(size_t i=0; i<n; i++)
        {
            matrix[i].reserve(n);
            restored_way.reserve(n);
        }
        fill_matrix(input_file);
    }
    input_file.close();
    return status_success;
}

status get_output(string &path_to_file, double probability, vector<size_t> &way)
{
    ofstream output_file(path_to_file);
    if(output_file.is_open())
    {
        output_file << probability << " " << way.size() << endl;
        for(auto &x: way)
            output_file << x << " ";
        output_file.close();
        return status_success;
    }
    else return status_output_file_error;
}


vector<bool> used;
vector<size_t> sorted;

void dfs(size_t vertex)
{
    used[vertex] = true;
    for(size_t i = 0; i < n; i++)
    {
        if (matrix[vertex][i] > 0)
        {
            size_t to = i;
            if(!used[to])
                dfs(to);
        }

    }
    sorted.emplace_back(vertex);
}

void topological_sort()
{
    used = vector<bool>(n, false);
    sorted.clear();
    for(size_t i = 0; i < n; i++)
        if(!used[i])
            dfs(i);
    reverse(sorted.begin(), sorted.end());
}

void reliable_way_finder(vector<vector<double>> &matrix, vector<size_t> &restored_way)
{
    std::vector<size_t>::iterator it = std::find(sorted.begin(), sorted.end(), 0);
    size_t i = std::distance(sorted.begin(), it);
    for(size_t k = i+1; k < n; ++k)
    {
        for(size_t j = k+1; j < n; ++j)
            if(matrix[sorted[i]][sorted[j]]<matrix[sorted[i]][sorted[k]]*matrix[sorted[k]][sorted[j]])
            {
                matrix[sorted[i]][sorted[j]] = matrix[sorted[i]][sorted[k]]*matrix[sorted[k]][sorted[j]];
                restored_way[sorted[j]] = sorted[k];
            }
    }
}

vector<size_t> answer_finder(vector<size_t> &restored_way)
{
    size_t elem = restored_way[n-1];
    vector<size_t> result;
    result.emplace_back(elem);
    elem = restored_way[elem];
    while (elem != 0)
    {
        result.emplace_back(elem);
        elem = restored_way[elem];
    }
    if(find(result.begin(), result.end(), 0) == result.end())
        result.emplace_back(0);
    reverse(result.begin(), result.end());
    result.emplace_back(n-1);
    return result;
}

int main(int argc, char **argv)
{
//    string path_to_input = argv[1];
//    string path_to_output = argv[2];
    string path_to_input = "input.txt";
    string path_to_output = "output.txt";
    get_input(path_to_input);
    topological_sort();
    reliable_way_finder(matrix, restored_way);
    vector<size_t> way = answer_finder(restored_way);
    cout << "way: "<< endl;
    double probability = matrix[0][n-1];
    get_output(path_to_output, probability, way);
    return 0;
}
